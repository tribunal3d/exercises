# -*- coding: utf-8 -*-
'''
Задание 1.4

Даны 3 переменные name age city

программа должна выводить строку:

Hello! My name is Ivan and Im 25 years old. Im from Moscow.

Напишите вывод строки минимум тремя способами
'''

name = 'Ivan'
age = '25'
city = 'Moscow'
print('Hello! My name is', name, 'and Im', age, 'years old. Im from', city, '.')
print('Hello! My name is {0} and Im {1} years old. Im from {2}.'.format(name, age, city))
print('Hello! My name is {name} and Im {age} years old. Im from {city}.'.format(name = 'Ivan', age = '25', city = 'Moscow'))
