# -*- coding: utf-8 -*-
"""
Задание 1.6

Обработать строку vova и вывести информацию на стандартный поток вывода в виде:
name:                  Владимир
ip:                    10.0.13.3
город:                 Moscow
date:                  15.03.2020
time:                  15:20

Ограничение: Все задания надо выполнять используя только пройденные темы.

"""

vova = 'O  Владимир      15.03.2020 15:20 10.0.13.3, 3d18h, Moscow/5'
vova = vova.replace(',', '')
q = vova[0:-2].split()
q = q[1:5]+q[6:7]
q[2], q[-1] = q[-1], q[2]
q[1], q[-2] = q[-2], q[1]
print('name:\t\t\t' + q[0] + '\n'
      + 'ip:\t\t\t\t' + q[1] + '\n'
      + 'город:\t\t\t' + q[2] + '\n'
      + 'date:\t\t\t' + q[3] + '\n'
      + 'time:\t\t\t' + q[4] + '\n')



