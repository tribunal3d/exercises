# -*- coding: utf-8 -*-
'''
Задание 2.1


В задании создан словарь, с информацией о разных серверах.

Необходимо запросить у пользователя ввод имени устройства (dev, qa или prod).
И вывести информацию о соответствующем устройстве на стандартный поток вывода
(информация будет в виде словаря).


Пример выполнения скрипта:
$ python task_2_1.py
Введите имя сервера: dev
{'location': 'Лубянка', 'vendor': 'IBM', 'model': 'i2570', 'os': 'centos', 'ip': '10.255.0.1'}

Ограничение: нельзя изменять словарь servers.

Все задания надо выполнять используя только пройденные темы.
То есть эту задачу можно решить без использования условия if.
'''

servers = {
    'dev': {
        'location': 'Лубянка',
        'vendor': 'IBM',
        'model': 'i2570',
        'os': 'centos',
        'ip': '10.255.0.1'
    },
    'qa': {
        'location': 'поеображенка',
        'vendor': 'Cisco',
        'model': '4451',
        'os': 'centos',
        'ip': '10.255.0.2'
    },
    'prod': {
        'location': 'технопарк',
        'vendor': 'Dell',
        'model': '3850',
        'os': 'centos',
        'ip': '10.255.0.101',
        'vlans': '10,20,30',
        'routing': True
    }
}

a = input('Введите имя сервера: ')
print(servers[a])

'''nums = ['10', '15', '20', '30', '100-200']
nums.reverse()
print(nums[::-1])
print(sorted(nums, reverse=True))

nums = ['10', '15', '20', '30', '100-200']
print(type(" ".join(nums)))

nums = ['10', '15', '20', '30']
nums.append('40')
print(nums)

num1 = ['10', '15', '20']
num2 = ['30', '45', '60']
num1.extend(num2)
print(num1)

#print(num1.pop(0))
# print(num1.remove('30'))
print(num1)

user = {'name': 'Петр', 'city': 'moscow', 'active': True}
print(user)'''