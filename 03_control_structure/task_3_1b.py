'''
Задание 3.1b
Скопировать код из предыдущего задания


Допишите код, чтобы:
    - после вывода результата код запускался заново и опять запрашивал ввод выражения;
    - при вводе пустой строки программа останавливалась.

'''

while True:
    calc = input('Введите выражение: ')
    if calc == '':
        break
    if '+' in calc:
        calc = (calc.split('+'))
        calc = int(calc[0]) + int(calc[1])
        print('Результат сложения: ' + str(calc))
    elif '-' in calc:
        calc = (calc.split('-'))
        calc = int(calc[0]) - int(calc[1])
        print('Результат вычитания: ' + str(calc))
    elif '*' in calc:
        calc = (calc.split('*'))
        calc = int(calc[0]) * int(calc[1])
        print('Результат умножения: ' + str(calc))
    elif '/' in calc:
        calc = (calc.split('/'))
        calc = int(calc[0]) / int(calc[1])
        print('Результат деления: ' + str(calc))
    else:
        print('Невалидное выражение ')